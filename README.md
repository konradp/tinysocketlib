
# Tinysocketlib
A small C++ class which uses UNIX network sockets. 
This is for use in my other projects. 
Currently this uses TCP sockets only.

## Usage
Take a look in `examples/` for example uses and compile instructions. 

## Troubleshooting
Listen on port 5000 with
```BASH
ncat -l 5000 -k -v
ncat -l 5000 -k -c 'xargs -n1 echo'
```

