#include <atomic>
#include <iostream>
#include <unistd.h>
#include <signal.h>
#include "lib/Tinysocketlib.hpp"

using namespace Tinysocketlib;
using std::cout;
using std::endl;
using std::string;
// TODO: Rename this
// TODO: Implement HttpTinypParser here for an example
// TODO: Link up, and finish
// TODO: Pass some JSON

class HttpParser : public Server {
    //~HttpParser() {
    //  std::cout << "Destructor" << std::endl;
    //}
    // Handler: This gets its own process
    void Handler(int sockfd) {
      int nbytes;
      char buffer[100];

      while (true) {
        int bytesIn = 0;
        // Receive data
        while((bytesIn = recv(sockfd, buffer, sizeof(buffer), 0)) > 0) {
          std::cout
            << "-- RECV: " << bytesIn << " bytes" << std::endl
            << BufToStr(buffer, bytesIn);
          // Send data back to client
          SendBuf(sockfd, buffer, bytesIn);

          string b = string(buffer, bytesIn);
          b.erase(b.find_last_not_of("\r\n") + 1); // trim newline

          // Close if exit requested
          if (b == "exit\r\n" || b == "exit") {
            SendStr(sockfd, "Client requested connection close.\n");
            close(sockfd);
            return;
          } else {
            // Do other stuff
          }
        }
        continue;
      } // while

      close(sockfd);
      exit(0);
    }; // Handler
}; // class HttpParser

// Call destructors on CTRL+C for clean socket close
std::atomic<bool> quit(false);
void got_signal(sig_atomic_t signal) {
  quit.store(true);
}

/* MAIN */
int main()
{
  HttpParser s;
  s.Run();
}

