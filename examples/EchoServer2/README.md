# A http parser
This is a minimal HTTP parser example which accepts TCP connections. 
To compile and run, do `./compile.sh`. 

## How it works
Most of the socket work is done by `lib/Tinysocketlib.hpp`. 
When you `include` the `Tinysocketlib.hpp`, instantiate the `Server`, and `Run()` it, this sets up a server, which listens on port 80. It accepts connections from more than one client. 
The `Handler()` method has been overriden, and it is where the receiving and sending is happening.

## Example run
Start the server:
```BASH
./compile.sh
```
In a separate terminal window, do
```BASH
telnet localhost 80
```

