#include <iostream>
#include "lib/Tinysocketlib.hpp"

using namespace Tinysocketlib;

class EchoServer : public Server {
    // Handler: This gets its own process
    void Handler(int file_desc) {
        std::cout << "Started echo handler: " << file_desc << std::endl;
        int nbytes;
        char buffer[10];
        
        while(1) {
            int bytesIn = 0;

            // Receive data
            while((bytesIn = recv(file_desc, buffer, sizeof(buffer), 0)) > 0) {
                std::cout
                    << "-- RECV: " << bytesIn << " bytes" << std::endl
                    << BufToStr(buffer, bytesIn) << std::endl
                    << "-- RECV: end" << std::endl;
                
                // Send data
                SendBuf(file_desc, buffer, bytesIn);

                // Close if exit requested
                if(std::string(buffer) == "exit\r\n") {
                    SendStr(file_desc, "Client requested connection close.\n");
                    close(file_desc);
                    exit(0);
                }
            }
            continue;

        } // while
        
        close(file_desc);
        exit(0);
    }; // Handler
}; // class EchoServer

/* MAIN */
int main()
{
    EchoServer s;
    s.Run();
}

