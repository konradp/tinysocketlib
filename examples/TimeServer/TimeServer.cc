#include <iostream>
#include <time.h>
#include "lib/Tinysocketlib.hpp"

using namespace Tinysocketlib;

class TimeServer : public Server {
    // Handler: This gets its own process
    void Handler(int file_desc) {
        std::cout << "Started time server: " << file_desc << std::endl;
        
        // Get time and send it
        time_t rawtime;
        time(&rawtime);
        SendStr(file_desc, std::string(ctime(&rawtime)));
        
        // Close connection
        close(file_desc);
        exit(0);
    }; // Handler
}; // class EchoServer

int main()
{
    TimeServer s;
    s.Run();
}

