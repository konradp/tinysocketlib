#include <iostream>
#include "lib/Tinysocketlib.hpp"

using namespace Tinysocketlib;

class EchoServer : public Server {
    // Handler: This gets its own process
    void Handler(int file_desc) {
        std::cout << "Started echo handler: " << file_desc << std::endl;
        int nbytes;
        char buffer[256];
        
        while(1) {
            // Receive
            nbytes = recv(file_desc, buffer, sizeof(buffer), 0);
            if(nbytes == 0) {
                continue; // No data received
            } else if(nbytes == sizeof(buffer)) {
                // Too much data received: close connection
                SendStr(file_desc,
                    "The data received exceeded the buffer size. Closing connection"
                );
                std::cout << "Closed connection. Too big for buffer" << std::endl;
                close(file_desc); // Close
            }

            // Data was good
            buffer[nbytes] = '\0'; // Remove garbage data from the buffer

            // Print out locally. Note: Disjointed output if no newline char received
            std::cout << std::string(buffer);

            // Echo back
            SendStr(file_desc, "Received: " + std::string(buffer));

            // Close if exit requested
            if(std::string(buffer) == "exit\r\n") {
                SendStr(file_desc, "Client requested connection close.\n");
                close(file_desc);
                exit(0);
            }
        } // while
        
        close(file_desc);
        exit(0);
    }; // Handler
}; // class EchoServer

/* MAIN */
int main()
{
    EchoServer s;
    s.Run();
}

