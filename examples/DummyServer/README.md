# A dummy server
This is a minimal dummy server example which does almost nothing besides accepting connections. 
To compile and run, do `./compile.sh`. 

## How it works
All the work is done by `Tinysocketlib.hpp`. 
When you `include` the `Tinysocketlib.hpp`, instantiate the `Server`, and `Run()` it, this sets up a server, which listens on port 80. It accepts connections from more than one client. 

## Example run
Start the server:
```BASH
./compile.sh
```
In a separate terminal window, do
```BASH
telnet localhost 80
```

