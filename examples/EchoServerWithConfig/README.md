# An echo server with config file
This is a minimal echo server example which accepts TCP connections. 
It then sends back to the client whatever the client has sent to the server. 
It also shows how to implement a config file, e.g. for controlling which port the server listens on. 
To compile and run, do `./compile.sh`. 

## How it works
Most of the work is done by `Tinysocketlib.hpp`. 
When you `include` the `Tinysocketlib.hpp`, instantiate the `Server`, and `Run()` it, this sets up a server, which listens on port 80. It accepts connections from more than one client. 
The `Handler()` method has been overriden, and it is where the receiving and sending is happening. 
You can change the port used from in the `config.cfg` file. 

## Example run
Start the server:
```BASH
./compile.sh
```
In a separate terminal window, do
```BASH
telnet localhost 80
```

