#include <fstream>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>

// TODO: Add some error checking
class ConfigRead {
public:
    ConfigRead() : configPath("./config.cfg") {};

    std::string GetValue(std::string _key) {
        std::ifstream ifs;
        ifs.open(configPath);
        std::string line, key, value;

        if(ifs.is_open()) {
            while(getline(ifs, line)) {
            //while(ifs >> key >> value) {
                std::istringstream s(line);
                s >> key >> value;
                // Ignore comments
                if(key == "#") continue;
                if(key == _key) return value;
            }
        }
        throw std::runtime_error("Value not found for key: " + _key);
    };

private:
    std::string configPath;
}; // class ConfigRead

