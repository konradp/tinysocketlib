#ifndef TINY_SOCKET_HPP
#define TINY_SOCKET_HPP
#include <iostream>
#include <netdb.h> // addrinfo()
#include <stdexcept> // exception definitions
#include <string.h> // memset()
#include <unistd.h> // fork(), close()
#include <arpa/inet.h> // inet_ntop()
#include <sys/types.h>
#include <sys/socket.h>

namespace Tinysocketlib {

using std::cout;
using std::endl;

/*class Handler {
public:
    Handler(int fd)
    : file_desc(fd) {};

    int Read(char* out) {
        int nbytes;
        nbytes = recv(file_desc, out, sizeof(out), 0);
        if(nbytes == 0) {
            return -1;
        }
        buf[nbytes] = '\0'; // terminate buffer
        return 0;
        
    };
private:
    int file_desc;
    char buf[10];
}; // class Handler
*/

class Server {
public:
    Server()
    : port(80) {};
    ~Server() {
      cout << "Closing" << endl;
    }
    
    void SetPort(int _port) { port = _port; };

    void Run() {
        std::cout << "Listening on port " << port << std::endl;

        // Load addr structs
        memset(&hints, 0, sizeof hints);
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;

        GetAddr();
        Socket();
        Bind();
        Listen();

        // WARN: Here we should reap zombie processes with sigaction

        // Main accept loop
        while(1) {
            int new_fd = Accept();
            if(new_fd == -1) continue;

            if(!fork()) { // the child process
                // This gets its own process
                Handler(new_fd);
            }

            // Close connection
            //std::cout << "Closing connection" << std::endl;
            close(new_fd);
        } // main accept loop

        //freeaddrinfo(servinfo);
    };
    
    void GetAddr() {
        int ok_addr = getaddrinfo(
            NULL,
            std::to_string(port).c_str(),
            &hints,
            &servinfo
        );

        if(ok_addr != 0) {
            std::cerr << "getaddrinfo: " << gai_strerror(ok_addr) << std::endl;
            exit(1);
        }
    };

    void Socket() {
        sockfd = socket(
            servinfo->ai_family,
            servinfo->ai_socktype,
            servinfo->ai_protocol
        );

        // Allow port reuse
        int ok_sockopt = setsockopt(
            sockfd,
            SOL_SOCKET,
            SO_REUSEADDR,
            &opt_reuseaddr,
            sizeof(int)
        );
        if(ok_sockopt != 0) {
            perror("setsockopt error");
            exit(1);
        }
    };

    void Bind() {
        int ok_bind = bind(
            sockfd,
            servinfo->ai_addr,
            servinfo->ai_addrlen
        );
        if(ok_bind != 0) {
            std::cerr
                << "bind: Could not bind to port " << port << ". " << std::endl
                << "bind: Is it already used by another application?"  << std::endl;
            exit(1);
        }
    };

    void Listen() {
        // Backlog = 10
        if(listen(sockfd, 10) != 0) {
            std::cerr << "Liten: error" << std::endl;
            exit(1);
        };
    };

    // Accept: Waits until incoming connection
    int Accept() {
        int file_desc;
        socklen_t addr_size = sizeof remote_addr;

        file_desc = accept(
            sockfd,
            (struct sockaddr *) &remote_addr,
            &addr_size
        );
        if(file_desc == -1) {
            std::cerr << "accept: error" << std::endl;
            return -1;
        }

        // Get client addr
        inet_ntop(
            remote_addr.ss_family,
            get_in_addr((struct sockaddr*) &remote_addr),
            s, sizeof s
        );
        std::cout << "Connection from " << s << "." << std::endl;
        return file_desc;
    };

    // Handler: Override this method to use your own handler
    // Otherwise, the default handler does nothing besides accepting connections
    virtual void Handler(int file_desc) {
        std::cout << "Started handler " << file_desc << std::endl;
        while(1) {
            // Wait for a second to avoid eating up CPU
            sleep(1);
        }
    };

    std::string BufToStr(char* buf, int buflen) {
        return std::string(buf, buflen);
    };

    void SendBuf(int sockfd, const void *buf, size_t len) {
        if(send(
                sockfd,
                buf,
                len,
                0
            ) == -1
        ) {
            std::cerr << "SendBuf: error";
            exit(1);
        }
    };

    void SendStr(int sockfd, std::string str) {
        if (send(
                sockfd,
                str.c_str(),
                str.length(),
                0
            ) == -1
        ) {
            std::cerr << "SendStr: error";
            exit(1);
        }
    };

private:
    void *get_in_addr(struct sockaddr *sa) {
        if(sa->sa_family == AF_INET) {
            return &(((struct sockaddr_in*)sa)->sin_addr);
        }
        return &(((struct sockaddr_in6*)sa)->sin6_addr);
    };
    // Socket structs
    struct addrinfo *servinfo;
    struct sockaddr_storage remote_addr;
    char s[INET6_ADDRSTRLEN];
    struct addrinfo hints;

    unsigned int port;
    int sockfd;
    int opt_reuseaddr;
}; // class Server

}; // end namespace Tinysocket
#endif //TINY_SOCKET_HPP
